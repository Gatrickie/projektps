#!/usr/bin/env python3
import itertools
import timeit
from random import randrange
from tabulate import tabulate
import numpy as np
from operator import itemgetter


# Create triangle upper [A] matrix in square matrix with given order and fill with pseudo-random numbers
def create_matrix_a(rows_n) -> np:
    ma = np.zeros((rows_n, rows_n))
    for i in range(0, rows_n, 1):
        for j in range(i, rows_n, 1):
            ma[i][j] = randrange(1, 9)
    return ma


# Create [B] matrix with given number of rows and fill with pseudo-random numbers
def create_matrix_b(rows_n) -> np:
    mb = np.random.rand(rows_n, 1)
    return mb


# Calculate Vertices and Arches for constructing graph
def graph_construction(ma, mb) -> None:
    number_of_nr = []

    graph_table = {}

    a = np.copy(ma)
    x = np.zeros((len(a), 1))
    b = np.copy(mb)

    W11 = []
    W12 = []
    Ix1 = []
    Ib1 = []
    Ia1 = []
    # first loop
    nr = 0
    for i in range(len(a), 0, -1):
        for j in range(i, i + 1):
            # x1[i] = (b1[i]) / (a[i][i])
            nr = nr + 1
            number_of_nr.append(nr)
            W11.append(i)
            W12.append(j)

            Ix1.append([i])
            Ib1.append([i])

            Ia1.append([i, i, '*'])

            graph_table['w{}'.format(nr)] = [(10 * j + i), i, j, i, i, (i, i), '/']

    W21 = []
    W22 = []
    Ix2 = []
    Ib2 = []
    Ia2 = []

    # second loop
    for i in range(len(a), 0, -1):
        for j in range(i - 1, 0, -1):
            # b[j] = b[j] - a[j][i] * x[i]
            nr = nr + 1
            number_of_nr.append(nr)
            W21.append(i)
            W22.append(j)

            Ix2.append([i])
            Ib2.append([j])

            Ia2.append([j, i])

            graph_table['w{}'.format(nr)] = [(10 * j + i), i, j, i, i, (j, i), '- *']

    graph_sorted = sorted(graph_table.values(), key=lambda kv: (kv[1], kv[0]))

    iter_temp = 1
    for i in graph_sorted:
        i[0] = iter_temp
        iter_temp = iter_temp + 1
    del iter_temp

    return nr, graph_sorted


# Solve matrix [X] from equation [A] * [X] = [B], with matrix [A] and [B] given
def solve_matrix(ma, mb) -> np:
    n = len(ma) - 1
    mx = np.zeros((len(ma), 1))
    mb_temp = np.copy(mb)

    start = timeit.default_timer()
    for i in range(n, -1, -1):
        mx[i] = (mb_temp[i]) / (ma[i][i])
        for j in range(i - 1, -1, -1):
            mb_temp[j] = mb_temp[j] - ma[j][i] * mx[i]
    stop = timeit.default_timer()
    exec_time = (stop - start) * 1000
    print("Execution time for N= {} is: {}ms".format(len(ma), exec_time))
    del mb_temp

    return mx


# Check correctness of solved [X]
def check_solution(ma, mb, mx) -> None:
    check = ma.dot(mx) == mb
    if check.all() == 1:
        print("Program is working fine!")
    else:
        print("Program failed!")

def tables(graph_table):
    print("\n----------Tablica pomocnicza----------")
    graph_table_headers_tab_pom = zip(*[['[nr]'], ['[W1]'], ['[W2]'], ['[Ib]'], ['[Ix]'], ['[Ia]'], ['[dzialanie]']])
    graph_table_tab_pom = itertools.chain(graph_table_headers_tab_pom, graph_table.copy())
    print(tabulate(graph_table_tab_pom))

    print("\n----------Tablica wezlow----------")
    graph_table_headers_tab_wez = zip(*[['[nr]'], ['[W1]'], ['[W2]'], ['[punkt]'], ['[dzialanie]']])
    nr = 0
    graph_table_short_tab_wez = graph_table.copy()
    for i in graph_table_short_tab_wez:
        del i[3], i[3]

    graph_table_tab_wez = itertools.chain(graph_table_headers_tab_wez, graph_table_short_tab_wez.copy())
    print(tabulate(graph_table_tab_wez))

    print("\n----------Tablica lukow----------")
    graph_table_copy = graph_table.copy()
    graph_table_copy_extend = []
    nr = 1

    for i in graph_table_copy:
        del i[-1]

    maks = []
    for i in graph_table_copy:
        maks.append(i[1])
    maks = max(maks)

    for i in graph_table_copy:
        if (i[1] != i[2]) and (i[1] != maks and i[2] != maks):
            graph_table_copy_extend.insert(-1, i)
            i.append(i[1] * 10 + i[2] * 100 + 1)
        else:
            i.append(i[1] * 10 + i[2] * 100)
        del i[0]
        del i[-1]
        nr = nr + 1

    graph_table_copy_merged = itertools.chain(graph_table_copy.copy(), graph_table_copy_extend.copy())
    graph_table_copy_merged_sorted = sorted(graph_table_copy_merged, key=itemgetter(-1))

    graph_final = []
    for i in graph_table_copy_merged_sorted:
        graph_final.append(i.copy())

    count = 0
    for i in graph_final:
        i.append('->')
        if (i[0] == i[1]) and ((i[1] < maks) or (i[0] < maks)):
            i.append('({}, {})'.format(i[1], i[0] + 1))
        elif (count == 0) and (i[0] != i[1]) and ((i[1] != maks) and (i[0] != maks)):
            i.append('({}, {})'.format(i[1], i[0] + 1))
            count = count + 1
        elif (count == 1) and (i[0] != i[1]) and ((i[1] != maks) and (i[0] != maks)):
            i.append('({}, {})'.format(i[1] + 1, i[0]))
            count = 0
        elif (i[0] == maks) and (i[0] != i[1]):
            i.append('({}, {})'.format(i[1] + 1, i[0]))

    graph_table_headers_tab_pom = zip(*[['[W1]'], ['[W2]'], ['[punkt1]'], ['->'], ['[punkt2]']])
    graph_table_tab_pom = itertools.chain(graph_table_headers_tab_pom, graph_final)
    print(tabulate(graph_table_tab_pom))

def main():
    rows = int(input("Input matrix root: "))
    matrix_a = create_matrix_a(rows)
    matrix_b = create_matrix_b(rows)
    matrix_x = solve_matrix(matrix_a, matrix_b)
    print('---MATRIX A---')
    print(matrix_a)
    print('-' * 35)
    print('---MATRIX B---')
    print(matrix_b)
    print('-' * 35)
    print('---MATRIX X---')
    print(matrix_x)
    print('-' * 35)
    print('-' * 35)

    nr, graph_table = graph_construction(matrix_a, matrix_b)
    tables(graph_table)

    closing_program = input('Waiting for keyboard input to close program\n'
                            'After input press ENTER')

if __name__ == "__main__":
    main()
